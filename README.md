# Install three.js
https://stackoverflow.com/questions/40273300/angular-cli-threejs#42602169

# Geometry
http://blog.cjgammon.com/threejs-geometry

# Fonts
https://github.com/mrdoob/three.js/tree/dev/examples/fonts  
http://gero3.github.io/facetype.js/

# Some links
[Руководство для новичка по Three.js](https://webdesign.tutsplus.com/ru/tutorials/a-noobs-guide-to-threejs--cms-28639)  
[Рисование толстых линий в WebGL](https://habr.com/ru/post/331164/)  
[Уроки по WebGL и three.js](https://dmitrylavrik.ru/blog/javascript/threejs)  
[THREEJS русская документация](http://www.ruthreejs.ru/)  
[Udemy. Lesson 15: Cameras](https://classroom.udacity.com/courses/cs291)
