import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as THREE from 'three';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild('rendererContainer') rendererContainer: ElementRef;
  renderer = new THREE.WebGLRenderer({alpha: true});
  scene = new THREE.Scene();
  camera;

  line;

  fontLoader = new THREE.FontLoader();

  clientWidth = 1000;
  clientHeight = 100;

  gridItemWidth = 60;
  halfOfGridItemWidth = this.gridItemWidth / 2;

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.renderer.setSize(this.clientWidth, this.clientHeight);
    this.rendererContainer.nativeElement.appendChild(this.renderer.domElement);

    this.fontLoader.load('assets/fonts/Lato_Regular.json', font => {

      // КАМЕРА
      this.camera = new THREE.OrthographicCamera(
        -this.halfOfGridItemWidth, this.clientWidth - this.halfOfGridItemWidth, // X, в сумме должно быть равно clientWidth
        -this.clientHeight / 2, this.clientHeight / 2, //                          Y, в сумме должно быть равно clientHeight
        -1, 1 //                                                                   Z, у нас плоскость, поэтому укажу хоть какой-то диапазон
      );
      this.camera.rotateX(Math.PI); // по умолчанию камера развернута на -180 градусов по X, приходится вращать обратно

      // Камера с перспективой неподходит из-за искажения пропорций
      // this.camera = new THREE.PerspectiveCamera(500, aspectRatio, 1, 1000);
      // this.camera.position.set(0, 0, 30);
      // this.camera.lookAt(0, 0, 10);


      // ЛИНИЯ ГРАФИКА
      const materialLine = new THREE.LineBasicMaterial({color: 0x0000ff});
      const geometryLine = new THREE.Geometry();
      geometryLine.vertices.push(new THREE.Vector3(0, 0, 0));
      geometryLine.vertices.push(new THREE.Vector3(60, 20, 0));
      geometryLine.vertices.push(new THREE.Vector3(120, -15, 0));
      geometryLine.vertices.push(new THREE.Vector3(180, 20, 0));
      geometryLine.vertices.push(new THREE.Vector3(240, 0, 0));
      geometryLine.vertices.push(new THREE.Vector3(300, 10, 0));
      geometryLine.vertices.push(new THREE.Vector3(360, -20, 0));
      geometryLine.vertices.push(new THREE.Vector3(420, 0, 0));
      geometryLine.vertices.push(new THREE.Vector3(480, -15, 0));
      geometryLine.vertices.push(new THREE.Vector3(540, 25, 0));
      geometryLine.vertices.push(new THREE.Vector3(660, 0, 0));
      geometryLine.vertices.push(new THREE.Vector3(720, 15, 0));
      geometryLine.vertices.push(new THREE.Vector3(780, -30, 0));
      geometryLine.vertices.push(new THREE.Vector3(840, 0, 0));
      geometryLine.vertices.push(new THREE.Vector3(900, -20, 0));
      geometryLine.vertices.push(new THREE.Vector3(960, 20, 0));
      this.line = new THREE.Line(geometryLine, materialLine);
      this.scene.add(this.line);


      // МАРКЕРЫ ТОЧЕК ГРАФИКА
      const geometry = new THREE.PlaneGeometry(5, 5);
      const material = new THREE.MeshBasicMaterial({color: 0x0000ff, side: THREE.FrontSide});
      let plane = new THREE.Mesh(geometry, material);
      plane.position.set(0, 0, 0);
      this.scene.add(plane);
      plane = new THREE.Mesh(geometry, material);
      plane.position.set(60, 20, 0);
      this.scene.add(plane);
      plane = new THREE.Mesh(geometry, material);
      plane.position.set(240, 0, 0);
      this.scene.add(plane);
      plane = new THREE.Mesh(geometry, material);
      plane.position.set(420, 0, 0);
      this.scene.add(plane);
      plane = new THREE.Mesh(geometry, material);
      plane.position.set(660, 0, 0);
      this.scene.add(plane);
      plane = new THREE.Mesh(geometry, material);
      plane.position.set(840, 0, 0);
      this.scene.add(plane);


      // НАДПИСИ НАД ТОЧКАМИ ГРАФИКА
      // https://github.com/mrdoob/three.js/tree/dev/examples/fonts
      const textGeo = new THREE.TextGeometry('10 C', {
        font,
        size: 12,
        height: 0,
      });
      const textMaterial = new THREE.MeshBasicMaterial({
        color: 0x0000ff,
        side: THREE.FrontSide
      });
      let text = new THREE.Mesh(textGeo, textMaterial);
      text.position.set(-15, 4, 0);
      this.scene.add(text);
      text = new THREE.Mesh(textGeo, textMaterial);
      text.position.set(45, 24, 0);
      this.scene.add(text);


      this.renderer.render(this.scene, this.camera);
    });
  }
}
